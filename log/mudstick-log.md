## 2020 06 07 

## Going BOM Notes

o-rings should be well under-sized, I have mcmaster PN 9452K136 for the time being, should use something smaller, for tube ID nominal 2" 

### With Gusto!

Today I'm designing a clay extruder. I've taken [one pass at this](https://gitlab.cba.mit.edu/jakeread/claystack) before, so I know a bit more about what I'd like to see in the end result:

clay should be easy to load,
straight-up plunging the clay is mechanically arduous, but worth it

I want to strong arm this whole situation. The mud will fear my plunger. From the supply chain I'm going to pull a few critical components:

closed loop gt2 for first reduction
bicycle headset bearings to mount
a ballscrew nut, driving
a ballscrew
nema 17 motor & 20 or 16T pinion pulley

I'll use 2.5" ID polycarb tubes to load clay in to, with 12" (off-the-shelf length) sections, assuming 8" used height, that's about 3.5lbs of clay, which is plenty much.

## 1kg Carry or Tube Delivery

My first temptation is to just carry this whole thing around the machine. The laying-down is speed-limited by the process, not by the machine's accelerations... so carrying 1kg on the end of the machine is OK. It's also straightforward, and has a kind of elegance that's hard to find with tubes.

I *am* a bit weary of doing this to the plastic parts of my [gantries](https://gitlab.cba.mit.edu/jakeread/rctgantries/). To that note, I'll also spec parts to design / build a 'bowden' type nozzle delivery... Since size will be a bonus there, I'll pick some ... 1/2" id hose and compression fittings. My notes from before:

```We found that with ~ 20% water added, we could push it through a 1/4" inside diameter tube having ~ 1m of length with only 30psi.```

So, straight up clay (no water), anticipating a similar (hopefully shorter) hose length ... 1/2" ID seems OK, considering the force (that I haven't actually calculated yet) that we should be able to plunge with.

## Design Log

Here's the stick at June 7 2019

![img](2019-06-07.png)

Ok, through some of this ...

 - nozzle detail ... o-ring, and screw-to-secure, or secure somehow ?
 - finish motor mount to pulley side
 - consider similar structure & method (alu tabs with 3dp heatset inserts to secure) for bottom lock-in ...
 - plate
 - fin
 - luer lock
 - vol / step ? haha

 - ballscrew: flip flange side,
 - instead of an o-ring seal, use dam-type flange wall?
 - or o-ring gets a nest, for slightly oversized o-rings ?

## The McMaster Order

 - 8585K31 1/8" Wall 2.5" ID Polycarb Tubes
 - 9558K75 O-Rings
 - bowden-type hose fittings ... barbs?
 - 50915K324 Compression Fitting 3/8" and 3/8"
 - 50915K195 Compression Sleeves
 - 50915K169 Compression Elbows (for turnaround possibility)
 - 4568K135 Turnaround Pipe

## 2021 06 30 

Eternal recurrence of the clay extrusion project here. Now there's clank-fxy and the hotplate, so it's going on that. 

Tiny downsize, I think this is going to be pretty cute. The design struggle is to see how we want to load / unload canisters of mud. A previous design had a little plunger-decoupler, so the rod came out, and the whole tube can come out w/ the plunger inside. I think I like this design... keeps the leadscrew out of the mud, etc. 

The height is large on that move, but we have the leadscrew. I think I might just detail it out and see what happens. 

Now, I want to say this is medium fancy. I guess it's a bummer if it costs dollars, but I could use a keyless bushing on the head of the leadscrew - that's a $60 part, but a really wonderful application for it. The ballscrew is $50, motor $20, controller, etc - they'd be $150 or so on-order. It's not so bad. 

And with a keyless bushing I can flip the leadscrew, opening lots of clearance for the tube pull-out. 

Making enough room for the pull-our really embiggens this thing. It wouldn't even help much to pull the whole plunger out. At the moment it means at least two prints for the wings that connect the load thru top-to-bottom. 

I guess I could just eat that, deal with the height. I could also try to unload differently - through the bottom. I could also shorten up the plunger: it's currently 0.75* the diameter of the tube for Saint Venant, could probably get away with a little less. 

Alright, pinched that off with a slightly under-spec lift out, can tilt the mf a little bit. 

There's a lot here left to do:

- structure
- load-in load-out clamp / whatever (?)
- o-ring spec
- lay-up toolpost 

I think I need to detail the toolpost before I can carry on with the structure on this thing; that means I need also the pogo boards. 

- draw board models, pogo and daughter
- integrate pogo into hotplate, 
- daughter, pins, bhcs into plate on plunger 
- wrap-around plates on plunger / main chassis
  - hug canister, allow for release 
  - don't interfere with clamp ... 

I've the board models in, need to bring the hotplate template into the plunger. 

## 2021 06 30 

Alright, tryna finish this thing today. Here's the current state: 

![prog](2021-06-30_progress.png)

That's got the hotplate pogo connector included. 

... wings

![wings](2021-06-30_wings.png)

That'll work, then. I think I would want to post a little handle on the canister, epoxied on. I also need to get one of these tubes to measure real ID / OD before I can print anything. 

I think I'd add a hang-up post etc later on. Here's what it looks like so far:

![stick](2021-06-30_fxy-stick.png)

It's actually not poorly proportioned w/r/t clank. I expect this would be about the largest thing we can fit on the machine without deforming flexures etc. 

OK I've a handle and o-ring spec, guesses - all undersized. I think it might be done (?). Last spec would be adding mount holes for future tool post. 

So I think that's it then. I'll put in the orders and carry on with other things. Challenges brining this online will be (1) whether this is enough torque or if we'll have to slip the clay, (2) whether the z-motors will be able to lift the thing or if we'll have to get closed loop motors running and (3) developing the workflow / UI, and (4) extending to multiple tool pickup / dropoff etc. Oh, and the pogo pins are new - that means also aligning D21 bus to the D51 bus and seeing whether the system can handle the contact during loading. 

### Orders

- keyless 10-23mm
- orings 
- more ballscrews, 4 u 