## Mudstick EE

[DEV LOG](log/mudstick-log.md) for updates.

Clay extruder, brute force plunger pusher. 

Used in the [clank/mudstick](https://github.com/jakeread/clank-mudstack) machine, residing at Haystack Mountain School of Crafts. 

![vid](log/2021-07_haystack-edit-enc.mp4)

![img](log/2021-07_clank-mudstack-5.jpg)

![img](log/2021-07_clank-mudstack-6.jpg)

![img](log/2021-07_clank-mudstack-11.jpg)